# Comment fonctionne la documentation ?

La documentation du cluster core de l'IFB s'appuie sur des pages au format Markdown. Ces pages sont transformés en contenu HTML à l'aide de l'outil MkDocs.

Chaque modification de la branche master du dépôt entraîne une regénération et publication automatique de la documentation.

# Comment contribuer ?

## Pour modifier une page de documentation

Cliquez sur le petit crayon en haut de la page à modifier (directement depuis le site de la documentation)

Editez le contenu de la page l'interface de GitLab.

Enregistrez vos modifications dans une nouvelle branche et créer immédiatement une MR vers la branche master.

## Pour ajouter une page de documentation

**Créer une nouvelle branche** sur le dépôt :

    $ git checkout -b <nom-branche>

**Créez un fichier** à la racine du projet pour une documentation en anglais.

Vous pouvez **tester votre nouveau contenu**, en exécutant MkDocs en local. Pour cela, placez-vous dans le dossier racine du dépôt, puis lancez

```
$ mkdocs serve
```

Le site est à présent consultable sur http://localhost:8000

Enfin **poussez votre nouvelle branche sur le dépôt** et effectuer une MR vers la branche master.

```
$ git push origin <nom-branche>
```
