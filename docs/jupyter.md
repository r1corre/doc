# Jupyter

The IFB Core Cluster integrates a JupyterHub service that let you spawn Jupyter servers over the cluster.

This service is accessible from a simple web browser by connecting with your IFB Core cluster account to https://jupyterhub.cluster.france-bioinformatique.fr

*You need to have at least one active project on the cluster to be able to spawn Jupyter servers*

## Add custom environment to the JupyterLab

To add custom environment for JupyterLab you need first to open a ssh session to the cluster (see [Logging in](logging-in.md)).

### Add a user-defined Python environment

Create a conda environment:

```
module load conda
conda create -n <your-env-name> python=3.7 ipykernel
```

Install any specific Python librairies in this environment...

Register this new environment for Jupyter :

```
~/.conda/envs/<your-env-name>/bin/python -m ipykernel install --user --name '<your-env-name>' --display-name "My Python"
```

### Add a user-defined R environment

Create a conda environment with R:

```
module load conda
conda create -n <your-env-name> r-base=3.6.3 r-irkernel jupyter_client
```

Install any specific R librairies in this environment...

Start R:

```
conda activate <your-env-name>
R
```

Register the R environment for Jupyter

```
IRkernel::installspec(name = '<your-env-name>', displayname = 'My R', user = TRUE)
```

### Add any existing conda R environment

For example R 4.0.2 already installed on the cluster.

Start R:
```
module load r/4.0.2
R
```

Register the R environment for Jupyter
```
IRkernel::installspec(name = 'r-4.0.2', displayname = 'Cluster R 4',  user = TRUE)
```
